#include "Physics.hpp"

Physics::Physics()
{
    vector_list = NULL;
};

void Physics::AddVector(Vector *vector)
{
    vector_list = recursive_Add(vector, vector_list);
}

p_vector_list Physics::recursive_Add(Vector *vector, p_vector_list head)
{
    if(head == NULL) 
    {
        p_vector_list tmp = new Vector_List;
        tmp->next = NULL;
        tmp->_vector = vector;
        return tmp;
    }
    head->next = this->recursive_Add(vector, head->next);
    return head;
}

Vector Physics::SumVector()
{
    return this->recursive_Calculate(vector_list);
}

Vector Physics::recursive_Calculate(p_vector_list head)
{
    if(head == NULL) return Vector(0,0);
    head->_vector->SumVector(recursive_Calculate(head->next));
    return *(head->_vector);   
}

void Physics::recursive_Clear(p_vector_list head)
{
    if(head == NULL) return;

    recursive_Clear(head->next);
    delete head;
    return;    
}

void Physics::ClearList()
{
    this->recursive_Clear(vector_list);
    vector_list = NULL;
}



void DrawVector(sf::RenderWindow &window, Vector *vector, sf::Vector2f start_position, sf::Color color)
{


    sf::Vertex line[] =
    {
        sf::Vertex(start_position, color),
        sf::Vertex(sf::Vector2f(start_position.x + sin(vector->getAng()) * vector->getMag(), start_position.y + cos(vector->getAng()) * vector->getMag()) , color)
    };

    window.draw(line, 2, sf::Lines);
    //std::cout<< line[1].position.x << " " << line[1].position.y << "\n";

}


void Physics::recursiveDraw(sf::RenderWindow &window, p_vector_list head, sf::Vector2f start_position, sf::Color color)
{
    if (head == NULL) return;

    DrawVector(window, head->_vector, start_position, color);

    recursiveDraw(window, head->next, start_position, color);
}

void Physics::DrawVectors(sf::RenderWindow &window, sf::Vector2f start_position)
{

    this->recursiveDraw(window, vector_list, start_position, sf::Color::Red);
    std::cout<<"-----------"<<"\n";

}



