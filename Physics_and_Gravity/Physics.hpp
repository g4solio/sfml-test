#ifndef PHYSICS_H
#define PHYSICS_H

#include <iostream>
#include "Vector2d.hpp"
#include <SFML/Graphics.hpp>


void DrawVector(sf::RenderWindow &window, Vector *vector, sf::Vector2f start_position, sf::Color color);

struct Vector_List
{
    Vector *_vector;
    Vector_List *next;
};
typedef Vector_List *p_vector_list;


class Physics
{
private:
    p_vector_list vector_list;

    p_vector_list recursive_Add(Vector *vector, p_vector_list head);
    Vector recursive_Calculate(p_vector_list head);
    void recursive_Clear(p_vector_list head);
    void recursiveDraw(sf::RenderWindow &window, p_vector_list head, sf::Vector2f start_position, sf::Color color);
public:
    Physics();
    void AddVector(Vector *vector);
    Vector SumVector();
    void ClearList();
    void DrawVectors(sf::RenderWindow &window, sf::Vector2f start_position);
};





#endif