#include "Vector2d.hpp"

Vector::Vector(float _direction, float _magnitude)
{

    this->x = _magnitude * cos(_direction);;
    this->y = _magnitude * sin(_direction);

};

float Vector::getMag() 
{ 
    return sqrt(x*x + y*y);  
};

float Vector::getAng() 
{ 
    return atan2(y ,x);      
};

void Vector::SumVector(Vector _vector)
{
    this->x += _vector.x;
    this->y += _vector.y;
};

Vector Vector::Inverted(float multiplier)
{

    return Vector(-x * multiplier, -y * multiplier, 0); 

};

Vector::Vector(float _x, float _y, int nothing)
{
    x = _x;
    y = _y;
}
