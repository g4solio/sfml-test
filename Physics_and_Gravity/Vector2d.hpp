#ifndef VECTOR_H
#define VECTOR_H

#include <cmath>

class Vector
{
private:
    float x;
    float y;
public:
    Vector(float direction, float magnitude);
    Vector(float _x, float _y, int nothing);
    float getMag();
    float getAng();
    void SumVector(Vector _vector);
    Vector Inverted(float multiplier);

};



#endif