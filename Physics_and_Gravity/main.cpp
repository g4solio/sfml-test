#include <SFML/Graphics.hpp>
#include <iostream>
#include <cmath>
#include "Physics.hpp"

using namespace std;





#define friction 0.0005
#define global_force_multipier 2.5
#define gravity 0.1

int main()
{


    Physics physics = Physics();
    sf::RenderWindow window(sf::VideoMode(1000, 900), "SFML works!", sf::Style::Default);
    sf::CircleShape shape(10.f);
    shape.setOrigin(10,10);

    //shape.setFillColor(sf::Color::Green);
    shape.setPosition(400,400);
    sf::Clock clock;
    sf::Time time;
    while (window.isOpen())
    {       
        window.clear();
        
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        time = clock.restart();

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
            physics.AddVector(new Vector(180 * 3.1415 / 180, 0.3 * global_force_multipier));
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
            physics.AddVector(new Vector(90 * 3.1415 / 180, 0.05 * global_force_multipier));
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
            physics.AddVector(new Vector(270 * 3.1415 / 180, 0.05 * global_force_multipier));
        
        //Apply gravity
        physics.AddVector(new Vector(0 * 3.1415 / 180, gravity * global_force_multipier));

        physics.DrawVectors(window, shape.getPosition());
        
        Vector vector = physics.SumVector();


        shape.setPosition(shape.getPosition().x + sin(vector.getAng()) * vector.getMag() * time.asSeconds(),
                        shape.getPosition().y + cos(vector.getAng()) * vector.getMag() * time.asSeconds());
        //cout << physics.SumVector().getMag() << endl;
        
        //DrawVector(window, &vector, shape.getPosition(), sf::Color::Red);        
        window.draw(shape);
        window.display();
        
        
        // re-add the last vector
        physics.ClearList();

        physics.AddVector(&vector);

        Vector tmp = vector.Inverted(friction);
        physics.AddVector(&tmp);
        

        //RESET
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
        {
            shape.setPosition(400,400);
            physics.ClearList();
        }
    }

    return 0;
}

