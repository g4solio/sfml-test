#include "Player.hpp"

Player::Player()
{
    _player = sf::RectangleShape(sf::Vector2f(w,h));
    _player.setPosition(310,300);
    _player.setOrigin(w/2,h/2);
}

void Player::getInput(float deltatime)
{
    //std::cout << deltatime << std::endl;
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
        this->MoveForward(deltatime);
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
        this->Rotate(-1, deltatime);
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
        this->Rotate(1, deltatime);

    
}

void Player::Rotate(int m, float deltatime)
{
    //std::cout << sin(_player.getRotation())<< " " << (int)_player.getRotation() << std::endl;
    _player.rotate(rotation * m * deltatime);
}

void Player::MoveForward(float deltatime)
{
    float rotation = _player.getRotation();
    rotation = rotation * 3.1415 / 180;
    //std::cout << rotation << std::endl;
    _player.move(std::sin(rotation) * acceleration * deltatime * -1, std::cos(rotation) * acceleration * deltatime);
}