#include <SFML/Graphics.hpp>

#include "Player.hpp"
int main()
{
    Player player = Player();




    

    sf::RenderWindow window(sf::VideoMode(800, 800), "SFML works!", sf::Style::Default);
    //sf::CircleShape shape(100.f);
    //shape.setFillColor(sf::Color::Green);

    sf::Clock clock;
    sf::Time time;
    while (window.isOpen())
    {       
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        time = clock.restart();
        player.getInput(time.asSeconds());
        window.clear();
        window.draw(player._player);
        window.display();
    }

    return 0;
    return 0;
}
