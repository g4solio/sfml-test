#ifndef PLAYER_H
#define PLAYER_H


#include <SFML/Graphics.hpp>
#include <iostream>
#include <cmath>

class Player
{

    protected:
        float rotation = 100;
        float acceleration = -100;
        int w = 10;
        int h = 20;

    public:
        void getInput(float deltatime);
        sf::RectangleShape _player;
        Player();
        void Rotate(int m, float deltatime);
        void MoveForward(float deltatime);

};


#endif