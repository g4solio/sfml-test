

#include <SFML/Graphics.hpp>
#include <iostream>

int main()
{

    sf::RenderWindow window(sf::VideoMode(200, 200), "SFML works!", sf::Style::Default);
    sf::Text text;
    sf::Font font;
    if(!font.loadFromFile("fonts/font.ttf"))
    {
        return 1;
    }
    text.setString("Hi man");
    text.setCharacterSize(24);  
    text.setFillColor(sf::Color::Red);  
    text.setFont(font);
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();
        window.draw(text);
        window.display();
    }

    return 0;
}