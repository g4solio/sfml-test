#ifndef TERRAIN_GENERATOR_H
#define TERRAIN_GENERATOR_H

#include<iostream>
#include <SFML/Graphics.hpp>
#include <stdlib.h>

struct Point
{
    int x;
    int y;
    Point *next;
};

typedef Point *p_point;

struct Lines
{
    std::vector<sf::Vertex> vertices;
    sf::PrimitiveType type;
};

class list_point
{
    protected:
        p_point head;
        p_point current_point;

        p_point recursive_Add(int x, int y, p_point h);

    public:

        list_point();
        
        void AddPoint(int x, int y);

        p_point GetPoint();

};

class Terrain
{

    protected:
        list_point *points;
        Lines *lines;
        void recursive_createLines(list_point *p);
    public:
        Terrain(int density, int max_h, int seed, int w);
        Lines GetLines();


};

#endif