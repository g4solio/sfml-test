
#include <SFML/Graphics.hpp>
#include <iostream>
#include "TerrainGenerator.hpp"

int main()
{
    
    Terrain *terrain = new Terrain(20, 130, 2, 800);
   

    sf::RenderWindow window(sf::VideoMode(800, 200), "SFML works!", sf::Style::Default);
    //sf::CircleShape shape(100.f);
    //shape.setFillColor(sf::Color::Green);

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();
        Lines lines = terrain->GetLines();
        window.draw(&lines.vertices[0], lines.vertices.size(), lines.type);
        window.display();
        int seed;
        std::cin >> seed;
        delete terrain;
        terrain = new Terrain(20, 130, seed, 800);
    }

    return 0;
}