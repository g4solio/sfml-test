#include "TerrainGenerator.hpp"

list_point::list_point()
{
    head = NULL;
    current_point = NULL;
}

void list_point::AddPoint(int x, int y)
{

    head = this->recursive_Add(x, y, head);
    current_point = head;
}

p_point list_point::GetPoint()
{

    if(current_point == NULL) return NULL;
    p_point tmp = current_point;
    current_point = current_point->next;
    return tmp;

}

p_point list_point::recursive_Add(int x, int y, p_point h)
{

    if(h == NULL) 
    {
        p_point tmp = new Point;
        tmp->x = x;
        tmp->y = y;
        tmp->next = NULL;
        return tmp;
    }
    h->next = recursive_Add(x, y, h->next);
    return h;

}

Lines Terrain::GetLines()
{
    recursive_createLines(points);
    return *lines;
}

void Terrain::recursive_createLines(list_point *p)
{
    p_point tmp = p->GetPoint();
    if(tmp == NULL) return;   
    lines->vertices.push_back(sf::Vector2f(tmp->x, tmp->y));
    std::cout << tmp->x << " " << tmp->y <<std::endl; 
    recursive_createLines(p);
}

Terrain::Terrain(int density, int max_h, int seed, int w)
{
    lines = new Lines;
    points = new list_point();
    lines->type = sf::LineStrip;
    srand(seed);
    
    for(double i = 0; i <= density; i++)
    {
        int y = rand()%max_h + 1;
        int x = (i/density) * w;
        points->AddPoint(x, y);
    }
    
}